const path = require('path');

const APIUrl = {
    local: {
        target: '',
        changeOrigin: true
    },
    remote: {
        targer: '',
        changeOrigin: true
    }
};

module.exports = {
    entry: {
        main: path.resolve(__dirname, '../src', 'index.tsx'),
        vendor: '@babel/polyfill'
    },
    output: {
        path: path.resolve(__dirname, '../dist'),
        filename: '[name]-[hash].js',
        publicPath: './'
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.jsx', '.css']
    },
    stats: { children: false },
    devServer: {
        historyApiFallback: true,
        host: '0.0.0.0',
        proxy: {
            // '/api': {
            //     target: 'http://localhost:8081',
            //     changeOrigin: true
            // },  
            'namespace': APIUrl.local
        }
    },
    module: {
        rules: [
            {
                test: /\.(j|t)sx?$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            cacheDirectory: true,
                        },
                    },
                    'astroturf/loader',
                    'react-hot-loader/webpack'
                ],
            },
            {
                test: /\.(woff|woff2|ttf)$/,
                use: {
                    loader: 'file-loader',
                    options: {
                        name: 'fonts/[name].[ext]',
                    },
                },
            },
            {
                test: /\.(png|jpg|gif|svg)$/,
                use: {
                    loader: 'file-loader',
                    options: {
                        name: 'img/[name].[ext]',
                    },
                },
            },
        ]
    }
};