# w15t
Webpack starter kit for UI

## Download
```bash
  git clone https://github.com/TRUEnov/w15t.git
```
## Install packages
```bash
  yarn install
```
## Run
```bash
  yarn start
```
## Test
```bash
  yarn test
```