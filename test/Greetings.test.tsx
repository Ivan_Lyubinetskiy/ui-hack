import * as React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';
import { Greetings, GreetingsWrapper } from '@@components';

describe('<Greetings /> tests', () => {
    let greetings: ShallowWrapper
    
    beforeEach(() => {
        greetings = shallow(<Greetings />);
    });
    afterEach(() => {
        greetings.unmount();
    });

    it('Correctly render', () => {
        expect(greetings.find(GreetingsWrapper)).toHaveLength(1);
        expect(greetings.find('h1').hasClass('header')).toBeTruthy();
        expect(greetings.find('h1').text()).toEqual('Hello world');
    });
});