import * as React from 'react';
import { render } from 'react-dom';
// Need to connect App to store
// import { Provider } from 'react-redux';
// import { store } from './ducks';
import App from '@@components/App';

import './global.css';

try {
    render(
        <App />,
        document.getElementById('app')
    );
} catch (e) {
    console.log(e);
}