import * as React from 'react';
import styled, { css } from 'astroturf';

export const GreetingsWrapper = styled.div`
    background: grey;
`;

const styles = css`
    .header { color: green; }
`;

export const Greetings: React.FC = () => (
    <GreetingsWrapper>
        <h1 className={styles.header}>Hello world</h1>
    </GreetingsWrapper>
);