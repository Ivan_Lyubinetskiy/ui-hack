import * as React from 'react';
import { Switch, Route, HashRouter } from 'react-router-dom';
import { hot } from 'react-hot-loader/root';
import { Greetings } from '@@components';
import LoginForm from './Login/Login';

const App = () => (
    <HashRouter>
        <Switch>
            <Route exact path="/" component={LoginForm}/>
        </Switch>
    </HashRouter>
);

export default hot(App);