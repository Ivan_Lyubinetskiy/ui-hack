module.exports = {
    setupFiles: ['<rootDir>/jest.setup.js'],
    preset: 'ts-jest',
    testEnvironment: 'node',
    transform: {
        '^.+\\.tsx?$': 'babel-jest',
        "^.+\\.svg$": 'jest-svg-transformer'
    },
    moduleNameMapper: { 
        "\\.(css|less|scss|sss|styl)$": "<rootDir>/node_modules/jest-css-modules", 
        "\\.(jpg|ico|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$": "<rootDir>/__mocks__/fileMock.js",
      
    },
    globals: { 
        'ts-jest': { 
            diagnostics: false
        } 
    },
};